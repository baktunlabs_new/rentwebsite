$('input[type="checkbox"]').on('change', function() {
  $(this).siblings('input[type="checkbox"]').not(this).prop('checked', false);
});


function getCoordinates () {
  return new Promise(function(resolve, reject) {
    navigator.geolocation.getCurrentPosition(resolve, reject);
  });
}
/*var a = moment([2007, 0, 29]);
    var b = moment([2007, 0, 28]);
    //a.diff(b, 'days')   // =1
    console.log(a.diff(b, 'days'))
 */
async function bookingSearch() {
  var l = null;
    var category = $('#categories').children("option:selected").val()
    var startDate = $('#startDate').val()
    var endDate = $('#endDate').val()
    var deliveryAddress = $('#deliveryAddress').val()
    var returnAddress = $('#returnAddress').val()
    var startDate2 = new Date(startDate)
    var endDate2 = new Date(endDate)
    var transmission = $("input[type=checkbox]:checked").val()

    days = moment(endDate2).diff(moment(startDate2), 'days')+1
    console.log(days);

    console.log(new Date(endDate2.setHours(0, 0, 0)))
    console.log(new Date(startDate2.setHours(0, 0, 0)))

    const { coords } = await getCoordinates()
    latitude = coords.latitude;
    longitude = coords.longitude;
    
   if(Object.entries(category).length === 0 || !endDate || !startDate || !deliveryAddress || !returnAddress){
      console.log('Ingrese todos los datos')
    } else {
        const results = await Parse.Cloud.run('bookingSearch', {
          startDate: startDate2,
          endDate: endDate2,
          categoryId: category,
          transmission: transmission,
          location: {latitude, longitude}
        })
        if (results) {
          window.location = '/cars'
          localStorage.setItem('deliveryAddress', deliveryAddress)
          localStorage.setItem('returnAddress',returnAddress)
          localStorage.setItem('days',days)
          localStorage.cars=JSON.stringify(results)
          console.log(startDate2)
        } else {
          console.log('no encontrado')
        }
      }
    }

