function initAutocomplete() {
    geocoder = new google.maps.Geocoder
    autocompleteDA = new google.maps.places.Autocomplete(
        document.getElementById('deliveryAddress'), {types: ['geocode']})
    
    autocompleteRA = new google.maps.places.Autocomplete(
        document.getElementById('returnAddress'), {types: ['geocode']})
    }

function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        geocoder.geocode({'location': geolocation}, function (results, status){
          if(status === 'OK'){
            if(results[0]){
              $('#deliveryAddress').val(results[0].formatted_address)
              //window.alert(results[0].formatted_address)
            } else {
              window.alert('no results found')
            }
          } else {
            window.alert('Geocoder failed due to: ' + status)
          }
        })
        console.log(geolocation)
      });
    }
  }
