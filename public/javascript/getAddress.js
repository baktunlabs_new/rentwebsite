searchPlaces = value => {
  const APIKEY = 'AIzaSyBkuwuqAQd0na7bYlaPkZ2U095-kfZ5xSg'
  const { latitude, longitude } = this.state
  fetch(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${value}&language=es&location=${latitude},${longitude}&radius=500&key=${APIKEY}`)
  .then(response => response.json())
  .then(result => {
    const { predictions } = result
    const data = predictions.map(x => {
      return {
        id: x.id,
        placeId: x.place_id,
        title: x.structured_formatting.main_text,
        subTitle: x.structured_formatting.secondary_text,
        alias: x.description
      }
    })
    // this.setState({ data })
  })
} 
navigator.geolocation.getCurrentPosition(position => {
  const { coords: { latitude, longitude } } = position
  const APIKEY = 'AIzaSyBkuwuqAQd0na7bYlaPkZ2U095-kfZ5xSg'
  fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${APIKEY}&sensor=true`)
    .then(response => response.json())
    .then(result => {
      if (result.status === 'OK') {
        const { results } = result
        const { formatted_address } = results[0] // <---- aca esta la direccion que se debe de ver en la vista
        //logica de como vas asignar esto a la vista
        $('#deliveryAddress').val(formatted_address)
      }
    })
})