
$(function () {
    // '+1d'
   var date = new Date(); 
    $('#startDate').datetimepicker({ 
        minDate:  moment().add(1, 'd').toDate(),
        ignoreReadonly: true,
     });
    $('#endDate').datetimepicker({ 
        minDate:  moment().add(1, 'd').toDate(),
        ignoreReadonly: true,
     });
    $('#startDate').on("change.datetimepicker", function (e) {
         var startDate = e.date._d
         var d = new Date(startDate)
         localStorage.setItem('startDate', startDate)
     });

     $('#endDate').on("change.datetimepicker", function (e) {
         var endDate = e.date._d
         localStorage.setItem('endDate', endDate)
     });
});

//startDate: moment().add('d', 1).toDate()