var cars = JSON.parse(localStorage.cars)
if (cars){
    console.log(localStorage.getItem("endDate") + ' ' + localStorage.getItem("startDate") + ' ' + localStorage.getItem("days"))
    console.log(cars)

    const output = cars.map(({id, brand, model, rate, acriss, photo, transmission, doors, services, type}) => {
            return `<div class="row mt-2 border-bottom">
            <div class="col-md-5">
                <h4 class="font-weight-bold">${brand} ${model}</h4>
                <p>
                <h5>Características</h5>
                    <span><img src="/img/icon/seat-belt.png"></span> ${acriss.pax}
                    <span><img src="/img/icon/suitcase.png"></span> ${acriss.largeSuitcase}
                    <span><img src="/img/icon/luggage.png"></span> ${acriss.smallSuitcase}
                    <span><img src="/img/icon/ice-crystal.png"> X
                    <span><img src="/img/icon/car-door.png"> ${doors}
                    <span><img src="/img/icon/automatic-transmission.png"></span> ${transmission} ${services}
                </p>
                <h5>Ventajas</h5>
                <p>
                    <span><img src="/img/icon/pet-friendly.png"></span> NO
                    <span><img src="/img/icon/car-wheel.png"></span> SI
                </p>
            </div>
            <div class="col-md-4">
            <img src="${photo}" alt="" class="img-fluid">
            </div>
            <div class="col-md-3">
                <p class="text-center">Precio / día</p>
                <h5 class="text-center font-weight-bold">$${rate.amount} mxn</h5>
                <center><button onclick="services('${type}', '${id}')" class="btn btn-primary btn-lg">Rentar</button></center>
            </div>
        </div>`}).join('')
        document.getElementById('cards').innerHTML = output
} else {
    console.log('PAGINA NO DISPONIBLE')
}

function services(type, id){
    var currentUser = Parse.User.current()
    if(currentUser){
        window.location = "/services/" + id + "/" + type 
    } else {
        window.location = "/login"
    }
    //
}

//onClick="location.href = '/services/${id}/${type}'" 
//window.location = "/services/" + id + "/" + type 