window.onload = function () {
    getCategory();
}
async function getCategory(){
  try{
    const query = new Parse.Query('Category');
    const results = await query.find()
    if(results) {
      results.forEach(element => {
        var name = element.get('name');
        $('#categories').append($('<option>', {value: element.id, text: name}))
      });
    }
  } catch (err) {
    console.log(err)
  }
}