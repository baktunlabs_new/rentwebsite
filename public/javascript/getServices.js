let showRegion = document.getElementById('region')
let driver = document.getElementById('driver')
let driver2 = document.getElementById('driver2')
let pickDate = localStorage.getItem("startDate")
let giveDate = localStorage.getItem("endDate")
let days = Number(localStorage.getItem("days"))
let results, services, checkedValue, outSide, addDriver, carId, servicesFinded, insurancesFinded
console.log(pickDate + ' ' + giveDate + ' ' + days)
window.onload = function () {
    getCarDataById()
    showRegion.style.display = "none"
    driver.style.display = "none"
    driver2.style.display = "none"
}

async function getCarDataById () {
    try{
        let url = await document.URL
        let urlParams = url.split('/')
       // let id = await url.substring(url.lastIndexOf('/') + 1)
       params = urlParams.slice(1).slice(-2)
        let id = params[0]
        let type = params[1]
        console.log(type)

        if(type == 'alied') {
            results = await Parse.Cloud.run('getCarAliedDataById', { carId:id })
        } else {
            results = await Parse.Cloud.run('getCarDataById', { carId:id })
        }

        console.log(results)
        //results1 = await Parse.Cloud.run('getCarAliedDataById', { carId: id})
        //results2 = await Parse.Cloud.run('getCarDataById', { carId:id })

        if(results) {
            services = await results.services
            insurances = await results.insurances
            priceDay = Number(results.rate.amount)

            const serv = services.map(x => {
                return `<div class="col-md-6">
                            <p class="border-bottom">${x.name} / precio del servicio $${x.price}.00 MXN</p>
                        </div>
                        <div class="col-md-6">
                            <label class="form-check-label" for="inlineCheckbox2"></label>
                            <input type="checkbox" class="messageCheckbox" id="${x.id}" name="services" value="">
                        </div>`}).join('')

            const ins = insurances.map(x => {
                return `<div class="col-md-6">
                            <p class="border-bottom">${x.name} / precio del servicio $${x.price}.00 MXN</p>
                        </div>
                        <div class="col-md-6">
                            <label class="form-check-label" for="inlineCheckbox2"></label>
                            <input type="checkbox" class="messageCheckbox" id="${x.id}" name="insurances" value="">
                        </div>`}).join('')
            document.getElementById('services').innerHTML = serv
            document.getElementById('insurances').innerHTML = ins
        }
    } catch (err) {
        console.log(err)
    }
}

function calculate(){
    let arraySer = []
    let arrayIns = []
    let serivcesPrice = 0
    let insurancesPrice = 0
    $("input[name=services]:checked").map(function () {
        const ids = [this.id]
        arraySer = [...arraySer, ...ids]
    })
    servicesFinded = services.filter(service => arraySer.includes(service.id))
    servicesFinded.map(x => {
        const inPrice = x.price * x.value
        serivcesPrice = serivcesPrice + inPrice
    });
    console.log(serivcesPrice)

    $("input[name=insurances]:checked").map(function () {
        const ids = [this.id]
        arrayIns = [...arrayIns, ...ids]
    })
    insurancesFinded = insurances.filter(insurance => arrayIns.includes(insurance.id))
    insurancesFinded.map(x => {
        insurancesPrice = insurancesPrice + Number(x.price)
    })
    console.log(insurancesPrice)
    const priceDays = priceDay * days
    const subTotal = priceDays + serivcesPrice + insurancesPrice
    const iva = subTotal * 0.16
    const total = subTotal + iva
    const payment = {
        total,
        iva,
        amortization: 0,
        serivcesPrice,
        insurancesPrice,
        priceDays,
        days,
        priceDay,
        }
    if(payment){
        console.log(payment)
        localStorage.setItem("paymentList", JSON.stringify(payment))
       /* window.location = "/documents"*/
    } else{
        console.log('not payment')
    }   
}
function create () {
    calculate();
    addRegion();
    addMoreDrivers();
    
    var region = $("#region").val()
    var firstDriver = $("#driver").val()
    var secondDriver = $("#driver2").val()

    var data = {
       // outSide: false,
        region,
        addDriver,
        firstDriver,
        //car: { id: results.id, owner: results.owner },
        secondDriver,
        services: servicesFinded,
        insurances: insurancesFinded,
        car: !results.alied ? results : null,
        carAlied: results.alied ? results : null
    }
    console.log(data)
    if(data){
        console.log(data)
        localStorage.setItem("dataList", JSON.stringify(data))
        window.location = "/documents"
    } else{
        console.log('not data')
    }
}

function addRegion () {
    var checkBox = document.getElementById("checkRegion")
    if (checkBox.checked == true){
      showRegion.style.display = "block"
      outSide = checkBox.checked
    } else {
       showRegion.style.display = "none"
       outSide = checkBox.checked
       document.getElementById('region').value = ''
    }
    console.log(outSide)
}

function addMoreDrivers () {
    var checkBox = document.getElementById("checkDriver")
    if (checkBox.checked == true){
        driver.style.display = "block"
        driver2.style.display = "block"
        addDriver = checkBox.checked
    } else {
        driver.style.display = "none"
        driver2.style.display = "none"
        addDriver = checkBox.checked
        document.getElementById('driver').value = ''
        document.getElementById('driver2').value = ''
    }
    console.log(addDriver)
}
