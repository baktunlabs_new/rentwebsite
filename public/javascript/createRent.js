var currentUser = Parse.User.current()
$('#address').val(currentUser.get('address'))
$('#city').val(currentUser.get('city'))
$('#state').val(currentUser.get('state'))
$('#curp').val(currentUser.get('curp'))
document.getElementById("ineCargado").style.display = "none";
document.getElementById("ineBackCargado").style.display = "none";
document.getElementById("driverLicenceCargando").style.display = "none";
//console.log(currentUser.get('driverLicence'))

console.log(currentUser.id)
var ine, driverLicence, ineBack

if (currentUser.has('driverLicence')){
  driverLicence = currentUser.get('driverLicence')
  document.getElementById("driverLicenceCargando").style.display = "block";
  console.log(driverLicence)
} else {
  console.log('no')
}

if (currentUser.has('ine')){
  ine = currentUser.get('ine')
  document.getElementById("ineCargado").style.display = "block";
  console.log(ine)
} else {
  console.log('no')
}

if (currentUser.has('ineBack')){
  ineBack = currentUser.get('ineBack')
  document.getElementById("ineBackCargado").style.display = "block";
  console.log(ineBack)
} else {
  console.log('no')
}

function encodeIne(element) {
    var file = element.files[0];
    //console.log(file)
    var reader = new FileReader();
    reader.onloadend = function() {
      ine = {
        fileName : file.name,
        type : file.type,
        uri : reader.result
      }
      console.log(ine)
    }
    reader.readAsDataURL(file);
}

function encodeIneBack(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    ineBack = {
      fileName : file.name,
      type : file.type,
      uri : reader.result
    }
    console.log(ineBack)
  }
  reader.readAsDataURL(file);
}

  function encodeDriverLicence(element) {
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function() {
      driverLicence = {
        fileName : file.name,
        type : file.type,
        uri : reader.result
      }
    console.log(driverLicence)
    }
    reader.readAsDataURL(file);
  }
async function createRent() {
    var pickDate = localStorage.getItem("startDate")
    var giveDate = localStorage.getItem("endDate")
    var giveBackLocation = localStorage.getItem("returnAddress")
    var pickUpLocation = localStorage.getItem("deliveryAddress")
    var payment = JSON.parse(localStorage.getItem("paymentList"))
    var data = JSON.parse(localStorage.getItem("dataList"))
    console.log(`${pickDate} ${giveDate} ${giveBackLocation} ${pickUpLocation}`)
    var address = document.getElementById('address').value
    var city = document.getElementById('city').value
    var state = document.getElementById('state').value
    var curp = document.getElementById('curp').value
    console.log(data)
    var documentation = {
        address,
        city,
        state,
        curp,
        ine,
        driverLicence,
        ineBack
    }
    console.log(documentation)
    /*var prueba = {
        userId: currentUser.id,
        documentation,
        payment,
        data,
        pickUpLocation,
        giveBackLocation,
        pickDate,
        giveDate
    }*/
    console.log(currentUser)
    try {

      if (!currentUser.get('isCompleted')) {
        const user = await Parse.Cloud.run('updateProfileInfo', {
          userId: currentUser.id,
          ...documentation
        })

        const rent = await Parse.Cloud.run('createRentWeb',  {
          userId: currentUser.id,
          documentation: {
            address: user.get('address'),
            city: user.get('city'),
            state: user.get('state'),
            curp: user.get('curp'),
            ine: user.get('ine'),
            ineBack: user.get('ineBack'),
            driverLicence: user.get('driverLicence')
          },
          payment,
          data,
          pickUpLocation,
          giveBackLocation,
          pickDate,
          giveDate
        })

        if(rent) {
          console.log('Renta creada')
        } else {
          console.log('Renta no creada')
        }
      } else {
        const rent = await Parse.Cloud.run('createRentWeb',  {
          userId: currentUser.id,
          documentation,
          payment,
          data,
          pickUpLocation,
          giveBackLocation,
          pickDate,
          giveDate
        })

        if(rent){
          console.log('renta realizada')
          localStorage.clear();
          window.location = "/successful";
        } else {
          console.log('renta no realizada')
        }
      }

      /*const rent = await Parse.Cloud.run('createRentWeb', {
        userId: currentUser.id,
        documentation,
        payment,
        data,
        pickUpLocation,
        giveBackLocation,
        pickDate,
        giveDate
      })*/

    } catch(error) {
      console.log(error)
    }
}